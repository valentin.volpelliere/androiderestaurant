package fr.isen.volpelliere.androiderestaurant.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ItemPanierJson(
        @SerializedName("plat") val plat:String,
        @SerializedName("quantity") var nbre : Int,
        @SerializedName("price") var price : Float,
        @SerializedName("img") val img : String
):Serializable
