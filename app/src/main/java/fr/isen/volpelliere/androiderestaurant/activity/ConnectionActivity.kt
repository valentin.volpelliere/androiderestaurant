package fr.isen.volpelliere.androiderestaurant.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fr.isen.volpelliere.androiderestaurant.databinding.ActivityConnectionBinding
import fr.isen.volpelliere.androiderestaurant.model.ClientJSON
import fr.isen.volpelliere.androiderestaurant.model.DataRegisterJSON
import org.json.JSONObject
import java.util.regex.Pattern

private  lateinit var binding: ActivityConnectionBinding
class ConnectionActivity : AppCompatActivity() {
    val gson: Gson = GsonBuilder().serializeNulls().create()
    private var requestQueue: RequestQueue? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConnectionBinding.inflate(layoutInflater)

        //Listener lors du click sur le bouton de connection
        binding.button.setOnClickListener{
            val email = binding.inputEmailc.text.toString()
            val password = binding.inputPassword.text.toString()
            val client = ClientJSON("","","","",email,password)
            val verif =  verificationForm(it,client)
            Log.d("Client",client.toString())
            if(verif) {
                login(client)
            }
        }

        //Listener lors du click sur le lien pour creer un nouveau compte
        binding.lienInscription.setOnClickListener {
            val intent = Intent(this,InscriptionActivity::class.java)
            startActivity(intent)
            finish()
        }

        setContentView(binding.root)
    }

    private fun verificationForm(view: View, client: ClientJSON):Boolean{
        if(client.email.isEmpty()){
            Toast.makeText(view.context, "Remplissez le champ Email", Toast.LENGTH_SHORT).show()
            return false
        }
        else{
            val emailRegex = Pattern.compile(
                    "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                            "\\@" +
                            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                            "(" +
                            "\\." +
                            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                            ")+")
            if(!emailRegex.matcher(client.email).matches()){
                Toast.makeText(view.context, "Verifié Syntaxe email Ex: Erestaurant@gmail.com", Toast.LENGTH_SHORT).show()
                return false
            }
        }
        if(client.password.isEmpty()){
            Toast.makeText(view.context, "Remplissez le champ Mot de passe", Toast.LENGTH_SHORT).show()
            return false
        }

        return true

    }

    private fun login(client:ClientJSON){
        val url = "http://test.api.catering.bluecodegames.com/user/login"
        requestQueue = Volley.newRequestQueue(this);
        val obj = JSONObject()
        obj.put("id_shop","1")
        obj.put("email",client.email)
        obj.put("password",client.password )
        val request = JsonObjectRequest(
                Request.Method.POST, url, obj,
                {
                    val code = it.getString("code")
                    val result = gson.fromJson(it.toString(), DataRegisterJSON::class.java)
                    Log.d("Code", code.toString())
                    if (code =="200") {
                     //   Log.d("DATA AFTER SERIALIZE", result.toString())
                        val sharedPreferences = getSharedPreferences("app_prefs", MODE_PRIVATE)
                        sharedPreferences.edit().putString("id_user", result.data.id).apply()
                        finish()
                    }
                    else {
                        Toast.makeText(applicationContext, "Mot de passe ou Email Incorrecte", Toast.LENGTH_SHORT).show()
                        Log.e("Connection","Mot de passe ou Email Incorrecte")
                    }
                },
                {
                    Log.e("ERROR",it.toString())
                    Toast.makeText(applicationContext, "Erreur envoie du formulaire", Toast.LENGTH_SHORT).show()
                })

        requestQueue?.add(request)

    }
}