package fr.isen.volpelliere.androiderestaurant.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataRegisterJSON(@SerializedName("data") val data: ClientJSON):Serializable
