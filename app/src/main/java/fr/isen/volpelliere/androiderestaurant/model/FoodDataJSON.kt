package fr.isen.volpelliere.androiderestaurant.model

import com.google.gson.annotations.SerializedName
import fr.isen.volpelliere.androiderestaurant.model.Food_Details_DataJSON
import java.io.Serializable

data class FoodDataJSON(
    @SerializedName("name_fr") val name: String,
    @SerializedName("items") val items: Array<Food_Details_DataJSON>

) : Serializable
