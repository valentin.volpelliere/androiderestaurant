package fr.isen.volpelliere.androiderestaurant.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataJSON(@SerializedName("data") val data: Array<FoodDataJSON>):Serializable
