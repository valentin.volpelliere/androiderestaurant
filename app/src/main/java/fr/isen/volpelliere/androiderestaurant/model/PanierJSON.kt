package fr.isen.volpelliere.androiderestaurant.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PanierJSON(
        @SerializedName("item") val item: ArrayList<ItemPanierJson>,
        @SerializedName("prixtotal") var price : Float
): Serializable
