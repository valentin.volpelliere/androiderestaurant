package fr.isen.volpelliere.androiderestaurant.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ClientJSON(
    @SerializedName("id") val id:String,
    @SerializedName("firstname") val firstname:String,
    @SerializedName("lastname") val lastname:String,
    @SerializedName("address")val address:String,
    @SerializedName("email") val email:String,
    @SerializedName("password")val password:String

):Serializable
