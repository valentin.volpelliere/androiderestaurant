package fr.isen.volpelliere.androiderestaurant.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.isen.volpelliere.androiderestaurant.activity.CellClickListener
import fr.isen.volpelliere.androiderestaurant.R
import fr.isen.volpelliere.androiderestaurant.databinding.PlatesCellBinding
import fr.isen.volpelliere.androiderestaurant.model.Food_Details_DataJSON


class RecycleAdapterFood(private val list_plats: Array<Food_Details_DataJSON>, private val cellClickListener: CellClickListener) : RecyclerView.Adapter<RecycleAdapterFood.ViewHolder?>() {

    class ViewHolder(binding: PlatesCellBinding) : RecyclerView.ViewHolder(binding.root) {
        val title = binding.plateName
        val img = binding.imgPlats
        val price = binding.priceLabel

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val contactView =PlatesCellBinding.inflate(inflater, parent, false)
        // Return a new holder instance
        return ViewHolder(contactView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        // Get the data model based on position
        val plats: String = list_plats[position].name_fr
        list_plats[position].getFormatedPrice()?.let{
            val price = it
            viewHolder.price.text = price
        }?: run {
            Log.e("ADAPTER","PAS DE PRIX TROUVES")
        }

        list_plats[position].getFirstPicture()?.let{
            val url = it;
            Log.d("DEBUG url",url)
            Picasso.get().load(url).placeholder(R.mipmap.load_image_foreground).into(viewHolder.img)
        }?: run{
            Log.e("ADAPTER","PAS D'IMAGE TROUVEES")
        }
        viewHolder.title.text = plats


       viewHolder.itemView.setOnClickListener {
            cellClickListener.onCellClickListener(list_plats[position])
        }


    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return list_plats.size
    }




}