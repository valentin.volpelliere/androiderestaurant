package fr.isen.volpelliere.androiderestaurant.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class IngredientsJSON(
    @SerializedName("id") val id :String,
    @SerializedName("id_shop") val id_shop :String,
    @SerializedName("name_fr") val name : String
):Serializable
