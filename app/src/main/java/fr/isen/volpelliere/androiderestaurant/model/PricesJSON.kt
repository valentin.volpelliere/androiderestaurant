package fr.isen.volpelliere.androiderestaurant.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PricesJSON (
    @SerializedName("id") val id:String,
    @SerializedName("price") val price:String
): Serializable