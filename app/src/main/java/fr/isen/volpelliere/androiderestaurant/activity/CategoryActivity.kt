package fr.isen.volpelliere.androiderestaurant.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fr.isen.volpelliere.androiderestaurant.adapter.RecycleAdapterFood
import fr.isen.volpelliere.androiderestaurant.databinding.ActivityCategoryBinding
import fr.isen.volpelliere.androiderestaurant.model.DataJSON
import fr.isen.volpelliere.androiderestaurant.model.Food_Details_DataJSON
import org.json.JSONObject


private lateinit var mRecyclerView : RecyclerView
private lateinit var mAdapterFood : RecyclerView.Adapter<RecycleAdapterFood.ViewHolder?>
private lateinit var mLayoutManager: RecyclerView.LayoutManager
private lateinit var binding: ActivityCategoryBinding

interface CellClickListener {
    fun onCellClickListener(details: Food_Details_DataJSON)
}


class CategoryActivity : BaseActivity(), CellClickListener {
    private var requestQueue: RequestQueue? = null
    val gson: Gson = GsonBuilder().serializeNulls().create()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCategoryBinding.inflate(layoutInflater)
        val intent = this.intent
        val category = intent.getStringExtra("Category")
        if (category != null) {
            binding.category.text= category
            sendAndRequestResponse(category)
        }

        setContentView(binding.root)
    }

    //Gestion du click sur les differentes cellules
    override fun onCellClickListener(details: Food_Details_DataJSON) {
        val intent = Intent(this, PlatsDetailsActivity::class.java)
        intent.putExtra(NAME, details.name_fr)
        if (details.imgs[0].isNotEmpty()) {
            intent.putExtra(IMG, details.imgs)
        }
        intent.putExtra(PRIX, details.getPrice())
        intent.putExtra(INGREDIENTS, details.getAllIngredients())
        startActivity(intent)
    }

    //Request vers le webservie pour récupérer les items correspondant à la catégorie passée en paramètre
    private fun sendAndRequestResponse(categorie: String) {
        val url = "http://test.api.catering.bluecodegames.com/menu"
        val obj = JSONObject()
        obj.put("id_shop", "1")
        requestQueue = Volley.newRequestQueue(this)
        val request = JsonObjectRequest(Request.Method.POST, url, obj,
            {
                val result = gson.fromJson(it.toString(), DataJSON::class.java)
              //  Log.d("DATA AFTER SERIALIZE",result.toString())
                result.data.firstOrNull{ it.name == categorie}?.items?.let{ plats->
                    display_details(plats)
                } ?: run {
                    Log.e("CategoryActivity","PAS DE PLATS TROUVEES")
                }

            },
            {
                Log.e("ERROR",it.toString())
            })

        requestQueue?.add(request)
    }

    //Permet d'afficher une liste de plats dans un RecyclerView
    private fun display_details(list_plats:Array<Food_Details_DataJSON>){
        mRecyclerView = binding.listPlats
        mLayoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = mLayoutManager
        mAdapterFood = RecycleAdapterFood(list_plats, this)
        mRecyclerView.adapter = mAdapterFood
    }
    companion object {
        const val NAME = "Name"
        const val PRIX = "Prix"
        const val IMG = "Img"
        const val INGREDIENTS = "Ingredients"
    }

}