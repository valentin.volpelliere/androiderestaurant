package fr.isen.volpelliere.androiderestaurant.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fr.isen.volpelliere.androiderestaurant.adapter.PagerAdapter
import fr.isen.volpelliere.androiderestaurant.databinding.ActivityPlatsDetailsBinding
import fr.isen.volpelliere.androiderestaurant.fragments.FragmentCaroussel
import fr.isen.volpelliere.androiderestaurant.model.ItemPanierJson
import fr.isen.volpelliere.androiderestaurant.model.PanierJSON
import java.io.File

private lateinit var binding: ActivityPlatsDetailsBinding
class PlatsDetailsActivity : BaseActivity() {
    val gson: Gson = GsonBuilder().serializeNulls().create()
    var panier: PanierJSON = PanierJSON(ArrayList(), 0F)
    var price_total: Float = 0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlatsDetailsBinding.inflate(layoutInflater)
        val intent = this.intent
        val name = intent.getStringExtra(NAME)
        val adapter = PagerAdapter(this)
        val img = intent.getStringArrayExtra(IMG)?.get(0)

        intent.getStringArrayExtra(IMG)?.let{
            it.forEach { img ->
                adapter.addFragment(FragmentCaroussel.newInstance(img))
            }
        }?: run{
            Log.e("DETAILS", "PAS D'IMAGE")
           adapter.addFragment(FragmentCaroussel.newInstance(null))
        }
        binding.caroussel.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.caroussel.adapter = adapter
        val price = intent.getStringExtra(PRIX)
        if (price != null) {
            price_total = price.toFloat()
        }
        val ingredient = intent.getStringExtra(INGREDIENTS)
        binding.detailsName.text= name
        binding.ingredients.text = ingredient
        binding.priceDetailLabel.text = "Total : $price €"

        //listener sur le bouton -
        binding.buttonMoins.setOnClickListener {
            if (binding.counter.text.toString().toFloat() > 1) {
                val counter = binding.counter.text.toString().toInt() - 1;
                binding.counter.text = counter.toString();
                if (price != null) {
                    updatePrice(price.toFloat(), counter)
                }
            }
        }

        //listener sur le bouton +
        binding.buttonPlus.setOnClickListener {
            val counter = binding.counter.text.toString().toInt() + 1;
            binding.counter.text = counter.toString();
            if (price != null) {
                updatePrice(price.toFloat(), counter)
            }
        }

        //listener sur le bouton de validation de la commande pour un item et l'ajoute au panier
        binding.priceDetailLabel.setOnClickListener {
            val file = File(cacheDir.absolutePath + "panier.json")
            if (file.exists()) {
                panier = gson.fromJson(file.readText(), PanierJSON::class.java)
            }
            if (price != null) {
                val item = if(img!=null) {
                    ItemPanierJson(
                            binding.detailsName.text as String,
                            binding.counter.text.toString().toInt(),
                            price.toFloat() * binding.counter.text.toString().toInt(),
                            img)
                } else{
                    ItemPanierJson(
                            binding.detailsName.text as String,
                            binding.counter.text.toString().toInt(),
                            price.toFloat() * binding.counter.text.toString().toInt(),
                            "")
                }
                if (!itemAlreadyExist(item, price.toFloat())) {
                    panier.item.add(item)
                }
            }
            panier.price += price_total
            val quantity_total = sumQuantity(panier.item)
            saveItemCount(quantity_total)
            val gson = gson.toJson(panier)
            file.writeText(gson)
            Snackbar.make(it, "Item Ajouté à votre panier!!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            mCartItemCount = quantity_total
            finish()
        }
        setContentView(binding.root)
    }

    //Permet de savoir si un item existe déja dans le panier. True si oui sinon false
    fun itemAlreadyExist(item: ItemPanierJson, price: Float): Boolean{
        panier.item.forEach { element ->
            if(element.plat == item.plat){
                element.price += item.nbre * price
                element.nbre += item.nbre
                return true
            }
        }
        return false
    }

    //Permet de sauvegarder le nombre d'item dans les préférences utilisateurs
    private fun saveItemCount(count:Int){
        val sharedPreferences = getSharedPreferences(APP_PREFS, MODE_PRIVATE)
        sharedPreferences.edit().putInt(PANIER_COUNT, count).apply()
    }

    //Permet d'update le prix en temps réel lors du changement de quantité d'un item
    fun updatePrice(price: Float, counter: Int){
        price_total = price * counter;
        val newprice = "Total : " +(price * counter).toString() + "€"
        binding.priceDetailLabel.text = newprice
    }

    // Calcule le nombre total d'item dans le panier
    fun sumQuantity(items: ArrayList<ItemPanierJson>) : Int{
        val somme = items.map { it.nbre }.sumBy { it }
        return somme
    }


    companion object {
        const val APP_PREFS =  "app_prefs"
        const val PANIER_COUNT = "panier_count"
        const val NAME = "Name"
        const val PRIX = "Prix"
        const val IMG = "Img"
        const val INGREDIENTS = "Ingredients"
    }
}