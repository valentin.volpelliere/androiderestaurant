package fr.isen.volpelliere.androiderestaurant.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import fr.isen.volpelliere.androiderestaurant.databinding.ActivityMainBinding




private lateinit var binding: ActivityMainBinding

class HomeActivity : BaseActivity() {


    private fun afficheToast(menu: TextView){
        when (menu.text.toString()){
            "Entrées" -> Toast.makeText(menu.context, "Entrées", Toast.LENGTH_SHORT).show()
            "Plats" -> Toast.makeText(menu.context, "Plats", Toast.LENGTH_SHORT).show()
            "Desserts" -> Toast.makeText(menu.context, "Desserts", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Listener sur le texte entrée
        binding.buttonEntree.setOnClickListener {
            afficheToast(it as TextView)
            val entreeActivity = Intent(this, CategoryActivity::class.java)
            entreeActivity.putExtra(CATEGORY, it.text.toString())
            startActivity(entreeActivity)

        }

        //Listener sur le texte plat
        binding.buttonPlat.setOnClickListener {
            afficheToast(it as TextView)
            val platActivity = Intent(this, CategoryActivity::class.java)
            platActivity.putExtra(CATEGORY, it.text.toString())
            startActivity(platActivity)
        }

        //Listener sur le texte dessert
        binding.buttonDesert.setOnClickListener {
            afficheToast(it as TextView)
            val desertActivity = Intent(this, CategoryActivity::class.java)
            desertActivity.putExtra(CATEGORY, it.text.toString())
            startActivity(desertActivity)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("HomeActivity", "destroyed")
    }

    companion object {
        const val CATEGORY = "Category"
    }
}