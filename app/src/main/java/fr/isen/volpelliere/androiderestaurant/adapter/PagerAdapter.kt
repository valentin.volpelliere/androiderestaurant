package fr.isen.volpelliere.androiderestaurant.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class PagerAdapter(fragactivity: AppCompatActivity): FragmentStateAdapter(fragactivity) {
    private val fragments: ArrayList<Fragment> = ArrayList()


    override fun getItemCount(): Int {
        return fragments.size;
    }

    fun addFragment(fragment: Fragment) {
        fragments.add(fragment)
    }

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }

}