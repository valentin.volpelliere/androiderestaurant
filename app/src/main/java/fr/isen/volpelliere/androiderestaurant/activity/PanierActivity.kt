package fr.isen.volpelliere.androiderestaurant.activity

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fr.isen.volpelliere.androiderestaurant.adapter.RecycleAdapterPanier
import fr.isen.volpelliere.androiderestaurant.databinding.ActivityPanierBinding
import fr.isen.volpelliere.androiderestaurant.model.ItemPanierJson
import fr.isen.volpelliere.androiderestaurant.model.PanierJSON
import org.json.JSONObject
import java.io.File

private lateinit var mRecyclerView : RecyclerView
private lateinit var mAdapterPanier : RecyclerView.Adapter<RecycleAdapterPanier.ViewHolder?>
private lateinit var mLayoutManager: RecyclerView.LayoutManager
private lateinit var binding: ActivityPanierBinding

interface BoutonSupprimeListener {
    fun onBoutonSupprimeClickListener(item: ItemPanierJson)
}

interface ItemListener {
    fun onBoutonPlusClickListener(item:ItemPanierJson)
    fun onBoutonMoinsClickListener(item:ItemPanierJson)
}


class PanierActivity : AppCompatActivity(),BoutonSupprimeListener,ItemListener{
    val gson: Gson = GsonBuilder().serializeNulls().create()
    private var requestQueue: RequestQueue? = null
    var panier:PanierJSON = PanierJSON(ArrayList(), 0F)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPanierBinding.inflate(layoutInflater)
        val panier = readJson()
        val id_user = getId_User()
        display_panier(panier.item)
        binding.buttonCommander.text = "Commander (Total " + panier.price.toString() +" €)"
        binding.loading.visibility = View.GONE
        binding.buttonCommander.setOnClickListener{
            if (id_user =="-1" || id_user ==null) {
                val itent_inscr = Intent(this, InscriptionActivity::class.java)
                startActivity(itent_inscr)
                finish()
            }
            else {
                if(panier.price> 0) {

                    binding.loading.visibility = View.VISIBLE
                    sendOrder(it, panier, id_user)



                }
                else {
                    Snackbar.make(it, "Votre Commande est vide !", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                }
            }
        }
        setContentView(binding.root)
    }

    //Permet de lire le fichier json contenant le panier si il existe
    private fun readJson() : PanierJSON {
        val file = File(cacheDir.absolutePath + "panier.json")
        if (file.exists()) {
            panier = gson.fromJson(file.readText(), PanierJSON::class.java)
        }

        return panier

    }

    //Permet d'afficher le contenu du panier dans le recycle view
    private fun display_panier(list_panier: ArrayList<ItemPanierJson>){
        mRecyclerView = binding.listPanier;
        mLayoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = mLayoutManager
        mAdapterPanier = RecycleAdapterPanier(list_panier, this,this)
        mRecyclerView.adapter = mAdapterPanier
    }

    //Fonction callback lors de la supression d'un item
    override fun onBoutonSupprimeClickListener(item: ItemPanierJson) {
        val file = File(cacheDir.absolutePath + "panier.json")
        panier.item.remove(item)
        panier.price = panier.price - item.price
        val gson = gson.toJson(panier)
        file.writeText(gson)
        val sharedPreferences = getSharedPreferences(PlatsDetailsActivity.APP_PREFS, MODE_PRIVATE)
        val count =  sharedPreferences.getInt("panier_count", 0) - item.nbre
        sharedPreferences.edit().putInt(PlatsDetailsActivity.PANIER_COUNT, count).apply()
        mRecyclerView.adapter = RecycleAdapterPanier(panier.item, this,this)
        mRecyclerView.adapter?.notifyDataSetChanged()
        binding.buttonCommander.text = "Commander (Total " + panier.price.toString() +" €)"

    }

    override fun onBoutonMoinsClickListener(item: ItemPanierJson) {
        val file = File(cacheDir.absolutePath + "panier.json")
        val index = panier.item.indexOf(item)
        panier.price -= panier.item[index].price/panier.item[index].nbre
        panier.item[index].nbre = item.nbre
        panier.item[index].price = item.price

        val gson = gson.toJson(panier)
        file.writeText(gson)
        val sharedPreferences = getSharedPreferences(PlatsDetailsActivity.APP_PREFS, MODE_PRIVATE)
        val count =  sharedPreferences.getInt("panier_count", 0) - 1
        sharedPreferences.edit().putInt(PlatsDetailsActivity.PANIER_COUNT, count).apply()
        mRecyclerView.adapter = RecycleAdapterPanier(panier.item, this,this)
        mRecyclerView.adapter?.notifyDataSetChanged()
        binding.buttonCommander.text = "Commander (Total " + panier.price.toString() +" €)"
    }

    override fun onBoutonPlusClickListener(item: ItemPanierJson) {
        val file = File(cacheDir.absolutePath + "panier.json")
        val index = panier.item.indexOf(item)
        panier.price += panier.item[index].price/panier.item[index].nbre
        panier.item[index].nbre = item.nbre
        panier.item[index].price = item.price
        val gson = gson.toJson(panier)
        file.writeText(gson)
        val sharedPreferences = getSharedPreferences(PlatsDetailsActivity.APP_PREFS, MODE_PRIVATE)
        val count =  sharedPreferences.getInt("panier_count", 0) + 1
        sharedPreferences.edit().putInt(PlatsDetailsActivity.PANIER_COUNT, count).apply()
        mRecyclerView.adapter = RecycleAdapterPanier(panier.item, this,this)
        mRecyclerView.adapter?.notifyDataSetChanged()
        binding.buttonCommander.text = "Commander (Total " + panier.price.toString() +" €)"
    }


    // Permet de remettre à 0 le panier
    private fun cleanPanier(){
        val file = File(cacheDir.absolutePath + "panier.json")
        panier.item.removeAll(panier.item)
        panier.price = 0F
        file.delete()
        val sharedPreferences = getSharedPreferences(PlatsDetailsActivity.APP_PREFS, MODE_PRIVATE)
        sharedPreferences.edit().putInt(PlatsDetailsActivity.PANIER_COUNT, 0).apply()
        mRecyclerView.adapter = RecycleAdapterPanier(panier.item, this,this)
        mRecyclerView.adapter?.notifyDataSetChanged()
        binding.buttonCommander.text = "Commander (Total " + panier.price.toString() +" €)"
    }
    //Permet de récupérer l'id du user se trouvant dans les préférences utilisateurs . Renvoit -1 sinon
    private fun getId_User(): String? {
        val sharedPref = getSharedPreferences("app_prefs", MODE_PRIVATE) ?: return null
        val id = sharedPref.getString("id_user", "-1")
        return id
    }

    //Permet d'envoter la commande au webservice
    private fun sendOrder(view: View, panier: PanierJSON, id_user: String) {
        val url = "http://test.api.catering.bluecodegames.com/user/order"
        requestQueue = Volley.newRequestQueue(this);
        val obj = JSONObject()
        obj.put("id_shop", "1")
        obj.put("id_user", id_user)
        obj.put("msg", panier)
        val request = JsonObjectRequest(
                Request.Method.POST, url, obj,
                {
                    if (it.get("code").toString() == "200") {
                        val timer = object:CountDownTimer(3000,1000){
                            override fun onTick(millisUntilFinished: Long) {
                                Log.d("LOADING", "WAIT")
                            }

                            override fun onFinish() {
                                binding.loading.visibility = View.GONE
                                Snackbar.make(view, "Votre Commande à bien été envoyée !", Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show()
                                cleanPanier()

                            }
                        }
                        timer.start()
                    } else {
                        Snackbar.make(view, "Erreur lors de l'envoie de la commande !", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                    }
                },
                {
                    Log.e("ERROR", it.toString())
                    Toast.makeText(applicationContext, "Erreur lors de l'envoie de la commande", Toast.LENGTH_SHORT).show()
                })

        requestQueue?.add(request)

    }

}