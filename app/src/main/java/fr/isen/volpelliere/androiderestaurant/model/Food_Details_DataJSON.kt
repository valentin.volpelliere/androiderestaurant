package fr.isen.volpelliere.androiderestaurant.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Food_Details_DataJSON(
    @SerializedName("id") val id:String,
    @SerializedName("name_fr") val name_fr:String,
    @SerializedName("id_category") val id_category : String,
    @SerializedName("categ_name_fr") val category_name : String,
    @SerializedName("images") val imgs : Array<String>,
    @SerializedName("ingredients") val ingredients : Array<IngredientsJSON>,
    @SerializedName("prices") val prices : Array<PricesJSON>
):Serializable {
    fun getFirstPicture() = if (imgs.isNotEmpty() && imgs[0].isNotEmpty()){
        imgs[0]
    }else {
        null
    }

    fun getAllIngredients(): String {
        var result= ""
        ingredients.forEachIndexed { index,it ->
            if(index != ingredients.size-1) {
                result = result + it.name + ","
            }
            else{
                result += it.name
            }
        }
        return result
    }

    fun getPrice() = if(prices.isNotEmpty() && prices[0].price.isNotEmpty()) {
        prices[0].price
    }else{
        null
    }

    fun getFormatedPrice() = if(prices.isNotEmpty() && prices[0].price.isNotEmpty()) {
        prices[0].price + "€"
    }else{
        null
    }
}
