package fr.isen.volpelliere.androiderestaurant.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import fr.isen.volpelliere.androiderestaurant.databinding.ActivityInscriptionBinding
import fr.isen.volpelliere.androiderestaurant.model.DataRegisterJSON
import fr.isen.volpelliere.androiderestaurant.model.ClientJSON
import org.json.JSONObject
import java.util.regex.Pattern.compile

private lateinit var binding: ActivityInscriptionBinding

class InscriptionActivity : AppCompatActivity() {
    val gson: Gson = GsonBuilder().serializeNulls().create()
    private var requestQueue: RequestQueue? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInscriptionBinding.inflate(layoutInflater)

        //Listener sur le bouton d'inscription
        binding.buttonInscription.setOnClickListener{
            val firstname = binding.inputPrenom.text.toString()
            val lastname = binding.inputNom.text.toString()
            val email = binding.inputEmail.text.toString()
            val address = binding.inputAdresse.text.toString()
            val mdp = binding.inputMdp.text.toString()

            val client = ClientJSON("",firstname,lastname,address,email,mdp)
            val verif = verificationForm(it,client)
            if(verif) {
                sendInscription(client)
            }
        }
        //Listener lors du click sur le lien pour accéder à l'activité de login
        binding.lienConnection.setOnClickListener{
            val intent = Intent(this,ConnectionActivity::class.java)
            startActivity(intent)
            finish()
        }
        setContentView(binding.root)
    }

    //Permet de vérifier le formulaire
    private fun verificationForm(view: View, client: ClientJSON):Boolean{
        if(client.lastname.isEmpty()){
            Toast.makeText(view.context, "Remplissez le champ Nom", Toast.LENGTH_SHORT).show()
            return false
        }
        if (client.firstname.isEmpty()){
            Toast.makeText(view.context, "Remplissez le champ Prénom", Toast.LENGTH_SHORT).show()
            return false
        }
        if(client.email.isEmpty()){
            Toast.makeText(view.context, "Remplissez le champ Email", Toast.LENGTH_SHORT).show()
            return false
        }
        else{
            val emailRegex = compile(
                "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                        "(" +
                        "\\." +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                        ")+")
            if(!emailRegex.matcher(client.email).matches()){
                Toast.makeText(view.context, "Verifié Syntaxe email Ex: Erestaurant@gmail.com", Toast.LENGTH_SHORT).show()
                return false
            }
        }
        if(client.address.isEmpty()){
            Toast.makeText(view.context, "Remplissez le champ Adresse", Toast.LENGTH_SHORT).show()
           return false
        }
        if(client.password.isEmpty()){
            Toast.makeText(view.context, "Remplissez le champ Mot de passe", Toast.LENGTH_SHORT).show()
            return false
        }
        else{
            if(client.password.length < 8){
                Toast.makeText(view.context, "Mot de passe doit faire minimum 8 caractères", Toast.LENGTH_SHORT).show()
            }
        }
        return true

    }

    //Permet d'envoyer les données de l'inscription au webservice
    private fun sendInscription(client: ClientJSON){
        val url = "http://test.api.catering.bluecodegames.com/user/register"
        requestQueue = Volley.newRequestQueue(this);
        val obj = JSONObject()
        obj.put("id_shop","1")
        obj.put("firstname",client.firstname)
        obj.put("lastname",client.lastname)
        obj.put("address",client.address)
        obj.put("email",client.email)
        obj.put("password",client.password )
        val request = JsonObjectRequest(
            Request.Method.POST, url, obj,
            {
                val result = gson.fromJson(it.toString(),DataRegisterJSON::class.java)
                val sharedPreferences = getSharedPreferences("app_prefs",MODE_PRIVATE)
                sharedPreferences.edit().putString("id_user",result.data.id).apply()
             //   Log.d("DATA AFTER SERIALIZE", result.toString())
                finish()
            },
            {
                Log.e("ERROR",it.toString())
                Toast.makeText(applicationContext, "Erreur envoie du formulaire", Toast.LENGTH_SHORT).show()
            })

        requestQueue?.add(request)
    }
}