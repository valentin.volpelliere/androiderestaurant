package fr.isen.volpelliere.androiderestaurant.activity

import android.content.Context
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import fr.isen.volpelliere.androiderestaurant.R


open class BaseActivity: AppCompatActivity() {
    var textCartItemCount: TextView? = null
    var mCartItemCount = 0

    //Creation du menu avec le panier
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_panier, menu)
        val menuItem = menu?.findItem(R.id.action_panier)

        val actionView = menuItem?.actionView
        textCartItemCount = actionView?.findViewById(R.id.cart_badge) as TextView
        setupBadge()

        //Listener sur l'icon du panier
        actionView.setOnClickListener{
            onOptionsItemSelected(menuItem)
        }

        return true

    }

    //On rafraichis le menu à chaque fois qu'on revient sur une activité possédant l'icon panier
    override fun onResume() {
        val sharedPref = this.getSharedPreferences("app_prefs", Context.MODE_PRIVATE) ?: return
        mCartItemCount = sharedPref.getInt("panier_count", 0)
        invalidateOptionsMenu()
        super.onResume()
    }

    //Lors du click sur l'icon redirection vers l'activité panier
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.action_panier -> {
                val panierActivity = Intent(this, PanierActivity::class.java)
                startActivity(panierActivity)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //Gestion de la pastille sur le panier
    private fun setupBadge() {
        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount!!.visibility != View.GONE) {
                    textCartItemCount!!.visibility = View.GONE
                }
            } else {
                textCartItemCount!!.text = Math.min(mCartItemCount, 99).toString()
                if (textCartItemCount!!.visibility != View.VISIBLE) {
                    textCartItemCount!!.visibility = View.VISIBLE
                    textCartItemCount!!.text
                }
            }
        }
    }



    fun setCartItemCount(count : Int){
        mCartItemCount = count
        invalidateOptionsMenu()
    }
}