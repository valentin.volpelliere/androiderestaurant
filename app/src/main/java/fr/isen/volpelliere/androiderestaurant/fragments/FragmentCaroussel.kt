package fr.isen.volpelliere.androiderestaurant.fragments

import android.R
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.squareup.picasso.Picasso
import fr.isen.volpelliere.androiderestaurant.databinding.FragmentCarouselBinding

private lateinit var binding: FragmentCarouselBinding
class FragmentCaroussel() : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = FragmentCarouselBinding.inflate(inflater,container,false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            if (it.getString("URL")!=null) {
                Picasso.get().load(it.getString("URL")).placeholder(fr.isen.volpelliere.androiderestaurant.R.mipmap.load_image_foreground)
                        .into(binding.imageDetail)
            }
        }
    }

    companion object {
        fun newInstance(url :String?): FragmentCaroussel {
            return FragmentCaroussel().apply { arguments = Bundle().apply { putString("URL",url) }}
        }
    }

}