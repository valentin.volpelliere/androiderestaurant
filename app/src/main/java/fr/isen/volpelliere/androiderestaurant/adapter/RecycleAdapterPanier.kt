package fr.isen.volpelliere.androiderestaurant.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.isen.volpelliere.androiderestaurant.R
import fr.isen.volpelliere.androiderestaurant.activity.BoutonSupprimeListener
import fr.isen.volpelliere.androiderestaurant.activity.ItemListener
import fr.isen.volpelliere.androiderestaurant.databinding.PanierCellBinding


import fr.isen.volpelliere.androiderestaurant.model.ItemPanierJson
class RecycleAdapterPanier(private val list_panier: ArrayList<ItemPanierJson>,private val boutonsupprimeListener: BoutonSupprimeListener,private val itemListener : ItemListener) : RecyclerView.Adapter<RecycleAdapterPanier.ViewHolder?>() {


    class ViewHolder(binding: PanierCellBinding) : RecyclerView.ViewHolder(binding.root) {
        val title = binding.labelNamePanier
        val img = binding.imgPanier
        val price = binding.priceItem
        val quantity = binding.quantity
        val bouton_suppr = binding.supprime
        val bouton_plus = binding.buttonPanierPlus
        val bouton_moins = binding.buttonPanierMoins
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val contactView = PanierCellBinding.inflate(inflater, parent, false)
        // Return a new holder instance
        return ViewHolder(contactView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val plats: String = list_panier[position].plat
        val price : String = list_panier[position].price.toString()
        val quantity : String = list_panier[position].nbre.toString()
        val img : String = list_panier[position].img

        if (img != "") {
            Picasso.get().load(img).placeholder(R.mipmap.load_image_foreground).into(holder.img)
        }
        holder.quantity.text = quantity
        holder.title.text = plats
        holder.price.text = "$price €"

        holder.bouton_suppr.setOnClickListener{
            boutonsupprimeListener.onBoutonSupprimeClickListener(list_panier[position])
        }

        holder.bouton_plus.setOnClickListener{
            holder.quantity.text = (holder.quantity.text.toString().toInt() + 1).toString()
            itemListener.onBoutonPlusClickListener(list_panier[position])
            list_panier[position].price +=  list_panier[position].price/list_panier[position].nbre
            list_panier[position].nbre +=1

        }

        holder.bouton_moins.setOnClickListener{
            if (holder.quantity.text.toString().toInt() > 1) {
                holder.quantity.text = (holder.quantity.text.toString().toInt() - 1).toString()
                list_panier[position].price -=  list_panier[position].price/list_panier[position].nbre
                list_panier[position].nbre -=1
                itemListener.onBoutonMoinsClickListener(list_panier[position])
            }
        }

    }

    override fun getItemCount(): Int {
        return list_panier.size
    }

}